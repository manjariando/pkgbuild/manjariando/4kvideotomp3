# Maintainer: Muflone http://www.muflone.com/contacts/english/

pkgname=4kvideotomp3
pkgver=3.0.0.930
pkgrel=2
pkgdesc="Extract audio in high-quality MP3 from any video"
arch=('x86_64')
url="https://www.4kdownload.com/products/product-videotomp3"
license=('custom:eula')
makedepends=('chrpath' 'imagemagick')
#install=${pkgname}.install
source=("${pkgname}_${pkgver}_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_${pkgver%.*}_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        'fix_symlink_path.patch')
sha256sums=('5eecbe6d963370c30cf2faa699a2926223902ac06ba9163b6a9733a9cf177c3b'
            '32b11e3cbeac76960ec3b13beaec32272aa29ca6204cb50ad2655321781d179c'
            '3ce937348abd12409d0736e1a30fb791ad697aaa3b07b8f8556d1fda0182ba6b'
            'f7892f717298a4cb00d2069fc928141f16b7bb0348db9f3d004c828ff20e9db8')

prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"

    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kvideotomp3_desktop="[Desktop Entry]
Name=4K Video to MP3
Name[pt_BR]=Vídeo 4K para MP3
GenericName=4K Video to MP3
GenericName[pt_BR]=Vídeo 4K para MP3
Comment=Convert any video to MP3 in one click
Comment[pt_BR]=Converta qualquer vídeo para MP3 com um clique
Exec=4kvideotomp3
Terminal=false
Type=Application
Icon=4kvideotomp3
Categories=AudioVideo;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kvideotomp3_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('openssl')

    # Install files
    install -m 755 -d "${pkgdir}/usr/lib"
    cp -r "${pkgname}" "${pkgdir}/usr/lib"
    chown root.root "${pkgdir}/usr/lib/${pkgname}"

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgname}/doc/eula"
    
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
